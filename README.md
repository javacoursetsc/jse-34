# TASK-MANAGER

Console application for task management

# DEVELOPER INFO

NAME: Anastasia Rubtsova

E-MAIL: Lafontana@mail.ru

Company: TSC

# SCREENSHOTS

https://yadi.sk/d/Q8nPo1Du-KoDpA?w=1

# SOFTWARE

* Java 1.8

* Windows OS

# HARDWARE

* RAM 16GB

* CPU i5

# HOW TO BUILD

```
mvn clean install
```

# HOW TO RUN

```
java -jar ./task-manager.jar
```
