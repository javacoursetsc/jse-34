package ru.arubtsova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserLogInCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "authorization.";
    }

    @Override
    public void execute() {
        System.out.println("Login:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSessionEndpoint().openSession(login, password);
        endpointLocator.setSession(session);
        System.out.println("You have been successfully authorised");
    }

}
