package ru.arubtsova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("About:");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
        System.out.println(Manifests.read("company"));
        //System.out.println("From Property Name: " + serviceLocator.getPropertyService().getDeveloperName());
        //System.out.println("From Property E-mail: " + serviceLocator.getPropertyService().getDeveloperEmail());
        //System.out.println("From Property Company: " + serviceLocator.getPropertyService().getDeveloperCompany());
    }

}
