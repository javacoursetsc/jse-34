package ru.arubtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.endpoint.Project;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Status;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by project name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Project:");
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        //final Project project = endpointLocator.getProjectEndpoint().findByNameProject(session, name);
        final Project projectStatusUpdate = endpointLocator.getProjectEndpoint().changeStatusByNameProject(session, name, status);
        Optional.ofNullable(projectStatusUpdate).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Project status was successfully updated");
    }

}
