package ru.arubtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.endpoint.Session;
import ru.arubtsova.tm.endpoint.Task;

import java.util.List;

public class TaskShowAllCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks, sort them.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("Task List:");
        @Nullable List<Task> tasks;
        tasks = endpointLocator.getTaskEndpoint().findAllTasksWithUserId(session);
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task.getName() + " - " + task.getDescription());
            index++;
        }
    }

}
