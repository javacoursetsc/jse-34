package ru.arubtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperCompany();

    @NotNull
    Integer getScannerInterval();

    @NotNull
    String getValue(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable String defaultValue
    );

    @NotNull
    String getValueString(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable String defaultValue
    );

    @NotNull
    Integer getValueInteger(
            @Nullable String javaOpts,
            @Nullable String environment,
            @Nullable Integer defaultValue
    );

}
