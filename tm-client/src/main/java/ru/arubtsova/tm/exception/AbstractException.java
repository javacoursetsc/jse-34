package ru.arubtsova.tm.exception;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public abstract class AbstractException extends RuntimeException {

    @Nullable
    protected String message;

    protected AbstractException(@Nullable String message) {
        this.message = message;
    }

}
