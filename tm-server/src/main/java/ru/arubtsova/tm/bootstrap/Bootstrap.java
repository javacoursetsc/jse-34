package ru.arubtsova.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.api.repository.ISessionRepository;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.api.service.*;
import ru.arubtsova.tm.component.Backup;
import ru.arubtsova.tm.endpoint.*;
import ru.arubtsova.tm.repository.ProjectRepository;
import ru.arubtsova.tm.repository.SessionRepository;
import ru.arubtsova.tm.repository.TaskRepository;
import ru.arubtsova.tm.repository.UserRepository;
import ru.arubtsova.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @NotNull
    private final IDomainService domainService = new DomainService(serviceLocator);

    @NotNull
    private final Backup backup = new Backup(propertyService, domainService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(serviceLocator);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(serviceLocator);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    private void initEndPoint() {
        registry(adminEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    public void run(@Nullable final String... args) {
        backup.load();
        backup.init();
        initEndPoint();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
