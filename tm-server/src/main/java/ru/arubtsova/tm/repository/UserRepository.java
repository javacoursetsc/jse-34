package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.IUserRepository;
import ru.arubtsova.tm.exception.entity.UserNotFoundException;
import ru.arubtsova.tm.model.User;

import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public static Predicate<User> predicateByLogin(@NotNull final String login) {
        return u -> login.equals(u.getLogin());
    }

    @NotNull
    public static Predicate<User> predicateByEmail(@NotNull final String email) {
        return u -> email.equals(u.getEmail());
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull User user = map.values().stream()
                .filter(predicateByLogin(login))
                .limit(1)
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull User user = map.values().stream()
                .filter(predicateByEmail(email))
                .limit(1)
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final User entity = findByLogin(login);
        remove(entity);
        return entity;
    }

}
