package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.exception.entity.UserNotFoundException;
import ru.arubtsova.tm.model.AbstractEntity;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected Map<String, E> map = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public void add(@NotNull final E entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public void addAll(@NotNull List<E> entities) {
        @NotNull final Map<String, E> map = entities.stream().collect(Collectors.toMap(E::getId, e -> e));
        this.map.putAll(map);
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) {
        @NotNull E entity = Optional.ofNullable(map.get(id)).orElseThrow(UserNotFoundException::new);
        return entity;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void remove(@NotNull final E entity) {
        map.remove(entity.getId());
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String id) {
        final E entity = findById(id);
        remove(entity);
        return entity;
    }

    public boolean contain(@NotNull final String id) {
        return map.containsKey(id);
    }

}
