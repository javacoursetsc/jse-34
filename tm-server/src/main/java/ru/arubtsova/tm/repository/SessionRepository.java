package ru.arubtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ISessionRepository;
import ru.arubtsova.tm.model.Session;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public Predicate<Session> predicateByUserId(@NotNull final String userId) {
        return session -> userId.equals(session.getUserId());
    }

    @Override
    public void exclude(@NotNull final Session session) {
        map.remove(session);
    }

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) {
        return findAll().stream()
                .filter(predicateByUserId(userId))
                .limit(1).findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<Session> findAllSession(@NotNull final String userId) {
        return map.values().stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

}
