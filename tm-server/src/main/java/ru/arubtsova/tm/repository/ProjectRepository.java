package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
