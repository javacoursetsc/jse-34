package ru.arubtsova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.service.IDomainService;
import ru.arubtsova.tm.api.service.IPropertyService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final IDomainService domainService;

    @NotNull
    private final IPropertyService propertyService;

    public Backup(
            @NotNull final IPropertyService propertyService,
            @NotNull final IDomainService domainService
    ) {
        this.propertyService = propertyService;
        this.domainService = domainService;
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, propertyService.getBackupInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void init() {
        load();
        start();
    }

    public void run() {
        domainService.saveBackup();
    }

    public void load() {
        domainService.loadBackup();
    }

}
