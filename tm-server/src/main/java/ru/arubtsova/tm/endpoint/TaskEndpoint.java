package ru.arubtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.ServiceLocator;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.model.Session;
import ru.arubtsova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public @NotNull List<Task> findAllTasksWithUserId(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    public @NotNull Task findTaskByIdWithUserId(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Task findByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Task findByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), name);
    }

    @WebMethod
    public void clearTasksWithUserId(
            @Nullable @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    public @NotNull Task updateByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public @NotNull Task updateById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public @NotNull Task startByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Task startById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Task startByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Task finishByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @NotNull Task finishById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishById(session.getUserId(), id);
    }

    @WebMethod
    public @NotNull Task finishByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @WebMethod
    public @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status);
    }

    @WebMethod
    public @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "status") final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @WebMethod
    public @Nullable Task removeTaskByIdWithUserId(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @WebMethod
    public @Nullable Task removeByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public @Nullable Task removeByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @WebMethod
    public @NotNull Task addTaskWithDescription(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(), name, description);
    }

}
