package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.service.ITaskService;
import ru.arubtsova.tm.exception.empty.EmptyNameException;
import ru.arubtsova.tm.exception.empty.EmptyUserIdException;
import ru.arubtsova.tm.model.Task;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

}
