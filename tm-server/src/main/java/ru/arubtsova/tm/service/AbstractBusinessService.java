package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.IBusinessRepository;
import ru.arubtsova.tm.api.IBusinessService;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.exception.empty.EmptyDescriptionException;
import ru.arubtsova.tm.exception.empty.EmptyIdException;
import ru.arubtsova.tm.exception.empty.EmptyNameException;
import ru.arubtsova.tm.exception.empty.EmptyUserIdException;
import ru.arubtsova.tm.exception.entity.ObjectNotFoundException;
import ru.arubtsova.tm.exception.system.IndexIncorrectException;
import ru.arubtsova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    private final IBusinessRepository<E> repository;

    public AbstractBusinessService(@NotNull IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Nullable
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator) {
        if (!Optional.ofNullable(comparator).isPresent()) return null;
        return repository.findAll(userId, comparator);
    }

    @Nullable
    public E add(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        return repository.add(userId, entity);
    }

    @NotNull
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @NotNull
    public E findByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    @NotNull
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @NotNull
    public E updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final E entity = findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    public E updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findById(userId, id);
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        entity.setId(id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    public E startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @NotNull
    public E startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @NotNull
    public E startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @NotNull
    public E finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @NotNull
    public E finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findById(userId, id);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @NotNull
    public E finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @NotNull
    public E changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(status);
        return entity;
    }

    @NotNull
    public E changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final E entity = findById(userId, id);
        entity.setStatus(status);
        return entity;
    }

    @NotNull
    public E changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @NotNull final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(status);
        return entity;
    }

    public void remove(@Nullable final String userId, @Nullable final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(entity).isPresent()) throw new ObjectNotFoundException();
        repository.remove(userId, entity);
    }

    @Nullable
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    @Nullable
    public E removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    @Nullable
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

}
