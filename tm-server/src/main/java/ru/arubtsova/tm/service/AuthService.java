package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.api.service.IAuthService;
import ru.arubtsova.tm.api.service.IPropertyService;
import ru.arubtsova.tm.api.service.IUserService;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.exception.authorization.AccessDeniedException;
import ru.arubtsova.tm.model.User;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRole(@Nullable Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final User user = getUser();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void register(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        userService.create(login, password, email);
    }

}
